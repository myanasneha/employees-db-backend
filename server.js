const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });
  
// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.post("/add/employee", (req, res) => {
  var EmployeeObj = new db.employees(req.body);
  EmployeeObj.save(function (error, resObj) {
    res.json({ resObj: resObj });
  });
});

app.get("/employees/list", (req, res) => {
  db.employees.find().exec(function (error, resultArray) {
    res.json({ employeesList: resultArray, employeesCount: resultArray.length });
  });
});

app.post("/edit/employee/:id", (req, res) => {
  db.employees.findOneAndUpdate({ empID: Number(req.params.id) }, req.body, { new: true }, function(err, resObj) {
    res.json({ resObj: resObj });
  });
});

app.post("/delete/employee/:id", (req, res) => {
  db.employees.deleteOne(
    { empID: Number(req.params.id) },
    function(err, resObj) {
      res.json({ resObj: resObj });
    });
});

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});