module.exports = mongoose => {
    const Employees = mongoose.model(
      "employees",
      mongoose.Schema(
        {
          empID: Number,
          isDeleted: false,
          empName: String,
          empdob: String,
          empDesignation: String,
          department: String,
          joiningDate: String,
          gender: String,
          experience: String,
          country: String
        },
        { timestamps: true }
      )
    );
    return Employees;
  };
